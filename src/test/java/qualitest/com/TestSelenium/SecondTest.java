package qualitest.com.TestSelenium;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class SecondTest {

	WebDriver driver;
	Properties prop;
	
	@BeforeClass
    public void runBeforeTestMethod() throws IOException {
        String mygecko=System.getenv("HOME") + "/Downloads/geckodriver";
        System.out.println(mygecko);
        System.setProperty("webdriver.gecko.driver",mygecko);
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        prop = new Properties();
  	    FileInputStream f = new FileInputStream("./data.properties");	  
  	    prop.load(f);	
  	    
  	    String myURL = prop.getProperty("url");
  	    
        //String myURL = System.getenv("tomcaturl");
        //String myURL = "https://www.google.com/";
        driver.get(myURL);
        try
        {
                Thread.sleep(3000);
        }
        catch(Exception e)
        {
                System.out.println(e.getMessage());
                driver.quit();
        }
    }
	
	@Test
	void testWeb() throws IOException {
		
	  //String text = "Hello WORLD";
	  String text = prop.getProperty("mytext");
	  int l = text.length();

	  String message = "Expected String " + text +  " not found";
	  String bodyText = driver.findElement(By.tagName("body")).getText();

	  //System.out.println("Body: " + bodyText.substring(0, 11 ) + ".");
      Assert.assertEquals(bodyText.substring(0, l),text, message);
      driver.quit();
	}
	@AfterClass
    public void runAfterTestMethod() {
      driver.quit();
    }
}
